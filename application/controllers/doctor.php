<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('m_doctor');
    }

	public function index() {
		$cek = $this->m_doctor->get_data();
        if ($cek > 0) {
            $data['data'] = $cek;
		}
		
		$data['content'] = "doctor/list";
        $this->load->view('main', $data);
	}

	function view_add() {
        $data['content'] = "doctor/add";
        $this->load->view('main', $data);
	}
	
	function action_add() {
        $name = $this->input->post('name');
        $special = $this->input->post('special');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$telephone = $this->input->post('phone');

        $data = array(
            'Name_Doctor' => $name,
            'Specialist' => $special,
            'City' => $city,
            'Address' => $address,
            'Phone_Number' => $telephone
        );

        $this->m_doctor->input_data($data, 'doctor');
        redirect(base_url('index.php/doctor/index'));
	}
	
	function view_edit($id) {
        $cek = $this->m_doctor->get_data_byid($id);
        if ($cek > 0) {
            $data['data'] = $cek;
        }

        $data['content'] = "doctor/edit";
        $this->load->view('main', $data);
	}
	
	function action_edit($id) {
        $name = $this->input->post('name');
        $special = $this->input->post('special');
		$address = $this->input->post('address');
		$city = $this->input->post('city');
		$telephone = $this->input->post('phone');

        $data = array(
            'Name_Doctor' => $name,
            'Specialist' => $special,
            'City' => $city,
            'Address' => $address,
            'Phone_Number' => $telephone
        );
		
        $where = array(
            'ID_Doctor' => $id
        );

        $this->m_doctor->update_data($where,$data,'doctor');

        redirect(base_url('index.php/doctor/index'));
    }

	function action_delete() {
		$id = $this->input->post('id', TRUE);
		// print_r($id);
        // die();
        // $id = $this->input->post('id');
        $where = array('ID_Doctor' => $id);

        $this->m_doctor->delete_data($where,'doctor');
        redirect(base_url('index.php/doctor/index'));
    }
}