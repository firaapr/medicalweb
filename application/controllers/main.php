<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
        parent::__construct();
        $this->load->model('m_dashboard');
    }

	// public function index() {
	// 	$cek = $this->m_doctor->get_data();
    //     if ($cek > 0) {
    //         $data['data'] = $cek;
	// 	}
		
	// 	$data['content'] = "doctor/list";
    //     $this->load->view('main', $data);
	// }
	public function index()
	{
		$cek = $this->m_dashboard->get_data();
        if ($cek > 0) {
            $data['data'] = $cek;
		}
		$data['content'] = "dashboard.php";
        $this->load->view('main', $data);
	}

	public function view_resep($id)
	{
		$cek = $this->m_dashboard->get_data_recipe($id);
        if ($cek > 0) {
            $data['data'] = $cek;
		}
		$data['content'] = "recipe.php";
        $this->load->view('main', $data);
	}
}
