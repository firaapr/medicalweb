<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('m_patient');
    }

	public function index() {
		$cek = $this->m_patient->get_data();
        if ($cek > 0) {
            $data['data'] = $cek;
		}
		
		$data['content'] = "patient/list";
        $this->load->view('main', $data);
	}

	function view_add() {
        $data['content'] = "patient/add";
        $this->load->view('main', $data);
	}
	
	function action_add() {
        $name = $this->input->post('name');
        $date = $this->input->post('date');
		$placeTTL = $this->input->post('placeTTL');
        $gender = $this->input->post('gender');
        $city = $this->input->post('city');
        $address = $this->input->post('address');
        $telephone = $this->input->post('telephone');
        $job = $this->input->post('job');

        $data = array(
            'Name_Patient' => $name,
            'Birthdate' => $date,
            'Birth_Place' => $placeTTL,
            'Gender' => $gender,
            'City' => $city,
            'Address' => $address,
            'Phone_Number' => $telephone,
            'Jobs' => $job,
        );

        $this->m_patient->input_data($data, 'patient');
        redirect(base_url('patient'));
	}
	
	function view_edit($id) {
        $cek = $this->m_patient->get_data_byid($id);
        if ($cek > 0) {
            $data['data'] = $cek;
        }

        $data['content'] = "patient/edit";
        $this->load->view('main', $data);
	}
	
	function action_edit($id) {
        $data = array(
            'Name_Patient' => $this->input->post('name'),
			'Birthdate' => $this->input->post('date'),
			'Birth_Place' => $this->input->post('placeTTL'),
			'Gender' => $this->input->post('gender'),
			'City' => $this->input->post('city'),
			'Address' => $this->input->post('address'),
			'Phone_Number' => $this->input->post('telephone'),
			'Jobs' => $this->input->post('job'),
		);
		
        $where = array(
            'ID_Patient' => $id
        );

        $this->m_patient->update_data($where,$data,'patient');
        redirect(base_url('patient'));
    }

	function action_delete() {
		$id = $this->input->post('id', TRUE);
		// print_r($id);
        // die();
        // $id = $this->input->post('id');
        $where = array('ID_Patient' => $id);

        $this->m_patient->delete_data($where,'patient');
        redirect(base_url('index.php/patient/index'));
    }
}