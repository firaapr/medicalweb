<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_login');
	}


	public function index()
	{
		// $data['message'] = "halooo";
		$data = array('message' => "");
		$in = $this->session->flashdata('message');
		if (isset($in)) {
			$data['message'] = $in;	
		}
		$this->load->view('login', $data);
		// $err = "haloo";
		// $this->load->view('login', $err);
	}

	public function login_post()
	{

		$username = $this->input->post('username');
		$pass = $this->input->post('password');

		// $cek = $this->m_login->get_data($username, $pass);
		// if ($cek > 0) {
		// 	$data_session = array(
		// 		'nama' => $username,
		// 		'status' => "login",
		// 		'role' => $cek[0]['RoleId'],
		// 		'id' => $cek[0]['Id'],
		// 		'phone' => $cek[0]['PhoneNumber'],
		// 		'address' => $cek[0]['Address'],
		// 	);

		// 	$this->session->set_userdata($data_session);

		// 	redirect(base_url("dashboard"));
		// 	// echo "Sukses";
		// } else {
		// 	echo "Username dan password salah !";
		// }

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data = $this->m_login->get_data_login($username, $password);
		if ($data) {
			$data_session = array(
				'username' => $username,
				'name' => $data[0]['Name_Receptionist'],
				'id' => $data[0]['ID_Receptionist']
			);

			$this->session->set_userdata($data_session);
			redirect(base_url('index.php/main/index'));
		} else {
			// $data = array('message' => "Username atau Password Salah");
			
			// $this->session->set_flashdata('data', $data);
			
			// redirect('login/form', 'refresh');
			$this->session->set_flashdata('message', 'Username atau Password Salah');
			// redirect("home/index");
			redirect(base_url());
			// $this->load->view('login', $data);
		}
	}
	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}