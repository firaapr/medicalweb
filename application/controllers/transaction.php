<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('m_transaction');
    }

	public function index() {
        $data['dataPatient'] = $this->m_transaction->get_patient();
        $data['dataDoctor'] = $this->m_transaction->get_doctor();
        $data['dataObat'] = $this->m_transaction->get_medicine();
		$data['content'] = "transaction/add";
        $this->load->view('main', $data);
    }
	
	function fetch_add() {
        $params = array(
            'Date' => $this->input->post('Date'),
            'ID_Patient' => $this->input->post('ID_Patient'),
            'ID_Doctor' => $this->input->post('ID_Doctor'),
            'ID_Receptionist' => $this->session->userdata("id"),
            'Diagnosis' => $this->input->post('Diagnosis'),
            'Diagnosis_Description' => $this->input->post('Diagnosis_Description'),
            'Price' => $this->input->post('Price'),
        );

        $lastId = $this->m_transaction->input_data_medis($params, 'medical_record');
        if ($lastId != 0) {
            $d =$this->input->post('dataObat');
            $dtArr = explode(",", $d);
            foreach ($dtArr as $v) {
                $id = explode("-", $v)[1];
                $total = explode("-", $v)[0];
                $this->m_transaction->input_data_resep($lastId, $id, $total);
            }
        }

        redirect(base_url('main'));
	}
}