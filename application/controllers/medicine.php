<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Medicine extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('m_medicine');
    }

	public function index() {
		$cek = $this->m_medicine->get_data();
        if ($cek > 0) {
            $data['data'] = $cek;
		}
		
		$data['content'] = "medicine/list";
        $this->load->view('main', $data);
	}

	function view_add() {
        $data['content'] = "medicine/add";
        $this->load->view('main', $data);
	}
	
	function action_add() {
        $params = array(
            'Category' => $this->input->post('Category'),
            'Name_Medicine' => $this->input->post('Name_Medicine'),
            'Expired_Date' => $this->input->post('Expired_Date'),
        );

        $this->m_medicine->input_data($params, 'medicine');
        redirect(base_url('index.php/medicine/index'));
	}
	
	function view_edit($id) {
        $cek = $this->m_medicine->get_data_byid($id);
        if ($cek > 0) {
            $data['data'] = $cek;
        }

        $data['content'] = "medicine/edit";
        $this->load->view('main', $data);
	}
	
	function action_edit($id) {
        $params = array(
            'Category' => $this->input->post('Category'),
            'Name_Medicine' => $this->input->post('Name_Medicine'),
            'Expired_Date' => $this->input->post('Expired_Date'),
        );
		
        $where = array(
            'ID_Medicine' => $id
        );

        $this->m_medicine->update_data($where,$params,'medicine');
        redirect(base_url('index.php/medicine/index'));
    }

    function action_delete() {
		$id = $this->input->post('id', TRUE);
		// print_r($id);
        // die();
        // $id = $this->input->post('id');
        $where = array('ID_Medicine' => $id);

        $this->m_medicine->delete_data($where,'medicine');
        redirect(base_url('index.php/medicine/index'));
    }

}