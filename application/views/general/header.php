<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <!-- <div class="logo-src"></div> -->
                <h5>REKAM MEDIS</h5>
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                            data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button"
                        class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>
            <div class="app-header__content">
                <div class="app-header-left">
                    
                </div>
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading">
                                        <?php echo $this->session->userdata("name"); ?>
                                    </div>
                                    <div class="widget-subheading">
                                        Resepsionis
                                    </div>
                                </div>
                                <div class="widget-content-right header-user-info ml-3">
                                <form action="<?= base_url() . 'index.php/login/logout' ?>">
                                    <button type="submit"
                                        class="btn-shadow p-1 btn btn-primary btn-sm">
                                        Logout
                                        <!-- <i class="fa text-white fa-sign-out pr-1 pl-1"></i> -->
                                    </button>
                                </form>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow">
                <div class="app-header__logo">
                    <div class="logo-src"></div>
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic"
                                data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button"
                            class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>
                <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            <li class="app-sidebar__heading">Dashboard</li>
                            <li>
                                <a href="<?= base_url() . 'index.php/main/index' ?>">
                                    <i class="metismenu-icon pe-7s-display2"></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="app-sidebar__heading">Data Master</li>
                            <li>
                                <a href="<?= base_url() . 'index.php/patient/index' ?>">
                                    <i class="metismenu-icon pe-7s-diamond"></i>
                                    Patient
                                    <i class="metismenu-state-icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() . 'index.php/medicine/index' ?>">
                                    <i class="metismenu-icon pe-7s-science"></i>
                                    Obat
                                    <i class="metismenu-state-icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url() . 'index.php/doctor/index' ?>">
                                    <i class="metismenu-icon pe-7s-id"></i>
                                    Dokter
                                    <i class="metismenu-state-icon"></i>
                                </a>
                            </li>
                            <li class="app-sidebar__heading">Transaksi</li>
                            <li>
                                <a href="<?= base_url().'index.php/transaction/index' ?>">
                                    <i class="metismenu-icon pe-7s-note2"></i>
                                    Rekam Medis
                                    <i class="metismenu-state-icon"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>