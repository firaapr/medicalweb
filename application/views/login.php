<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Medical</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description"
        content="Wide selection of cards with multiple styles, borders, actions and hover effects.">
    <meta name="msapplication-tap-highlight" content="no">
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <link href="assets/css/main.css" rel="stylesheet">
    <style>
    .margin-login {
        max-width: 500px;
        margin: 0 auto;
        padding: 10;
    }

    .margin-button {
        margin-top: 30px;
    }

    .card-padding {
        padding: 100px;
    }
    </style>
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-main margin-login">
            <div class="tab-content">
                <div class="tab-pane tabs-animation fade show active" id="tab-content-1" role="tabpanel">
                    <div class="row">
                        <div class="main-card mb-3 card card-padding">
                            <div class="card-header">REKAM MEDIS</div>
                            <div class="card-body">
                                <h2>LOGIN</h2>
                                <form class="needs-validation"
                                    action="<?php echo base_url('index.php/login/login_post'); ?>" method="post"
                                    novalidate>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label for="validationCustom01">Username</label>
                                            <input type="text" class="form-control" id="validationCustom01"
                                                placeholder="Masukkan username" value="" name="username" required>
                                            <div class="invalid-feedback">
                                                Masukkan username
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row margin-button">
                                        <div class="col-md-12">
                                            <label for="validationCustom02">Password</label>
                                            <input type="password" class="form-control" id="validationCustom02"
                                                placeholder="Masukkan password" value="" name="password" required>
                                            <div class="invalid-feedback">
                                                Masukkan password
                                            </div>
                                        </div>
                                    </div>


                                    <?php if (isset($message) != false && empty($message) == false) {?>
                                    <h6 class="margin-button" style="color:red"><?= $message;?> </h6>
                                    <?php } ?>


                                    <button class="btn btn-primary margin-button" type="submit">LOGIN</button>

                                </form>


                                <script>
                                // Example starter JavaScript for disabling form submissions if there are invalid fields
                                (function() {
                                    'use strict';
                                    window.addEventListener('load', function() {
                                        // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                        var forms = document.getElementsByClassName('needs-validation');
                                        // Loop over them and prevent submission
                                        var validation = Array.prototype.filter.call(forms, function(form) {
                                            form.addEventListener('submit', function(event) {
                                                if (form.checkValidity() === false) {
                                                    event.preventDefault();
                                                    event.stopPropagation();
                                                }
                                                form.classList.add('was-validated');
                                            }, false);
                                        });
                                    }, false);
                                })();
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="assets/scripts/main.js"></script>

</html>