<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-anchor icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>UBAH DATA PASIEN</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form class="needs-validation"
                            action="<?php echo base_url('patient/action_edit/'.$data[0]['ID_Patient']); ?>"
                            method="post" novalidate>
                            <div class="form-row">
                                <div class="col-md-11 mb-3">
                                    <label for="validationCustom01">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="validationCustom01"
                                        placeholder="Masukkan nama lengkap" value="<?= $data[0]['Name_Patient'] ?>"
                                        name="name" required>
                                    <div class="invalid-feedback">
                                        Masukkan nama lengkap
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Tanggal Lahir</label>
                                    <input type="date" class="form-control" id="validationCustom02" placeholder=""
                                        value="<?= $data[0]['Birthdate'] ?>" name="date" required>
                                    <div class="invalid-feedback">
                                        Masukkan tanggal lahir
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3"></div>
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom03">Tempat Tanggal Lahir</label>
                                    <input type="text" class="form-control" id="validationCustom03"
                                        placeholder="Masukkan kota TTL" value="<?= $data[0]['Birth_Place'] ?>"
                                        name="placeTTL" required>
                                    <div class="invalid-feedback">
                                        Masukkan tempat tanggal lahir
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom04">Jenis Kelamin</label>
                                    <div>
                                        <div class="custom-radio custom-control">
                                            <input type="radio" id="exampleCustomRadio" name="gender"
                                                class="custom-control-input" value="Wanita"
                                                <?php if($data[0]['Gender'] === 'Wanita') echo 'checked="checked"';?>>
                                            <label class="custom-control-label" for="exampleCustomRadio">
                                                Wanita
                                            </label>
                                        </div>
                                        <div class="custom-radio custom-control">
                                            <input type="radio" id="exampleCustomRadio2" name="gender"
                                                class="custom-control-input" value="Pria"
                                                <?php if($data[0]['Gender'] === 'Pria') echo 'checked="checked"';?>>
                                            <label class="custom-control-label" for="exampleCustomRadio2">
                                                Pria
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom04">Kota</label>
                                    <input type="text" class="form-control" id="validationCustom04"
                                        placeholder="Masukkan kota asal" value="<?= $data[0]['City'] ?>" name="city"
                                        required>
                                    <div class="invalid-feedback">
                                        Masukkan kota asal
                                    </div>
                                </div>
                                <div class="col-md-1 mb-3"></div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom04">Alamat</label>
                                    <input type="text" class="form-control" id="validationCustom04"
                                        placeholder="Masukkan alamat lengkap" value="<?= $data[0]['Address'] ?>"
                                        name="address" required>
                                    <div class="invalid-feedback">
                                        Masukkan alamat lengkap
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Telepon</label>
                                    <input type="text" class="form-control" id="validationCustom02"
                                        placeholder="Masukkan nomor telepon" value="<?= $data[0]['Phone_Number'] ?>"
                                        name="telephone" required>
                                    <div class="invalid-feedback">
                                        Masukkan nomor telepon
                                    </div>
                                </div>
                                <div class="col-md-1 mb-3"></div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom03">Pekerjaan</label>
                                    <input type="text" class="form-control" id="validationCustom03"
                                        placeholder="Masukkan perkerjaan pasien" value="<?= $data[0]['Jobs'] ?>"
                                        name="job" required>
                                    <div class="invalid-feedback">
                                        Masukkan pekerjaan
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Simpan Data</button>
                        </form>

                        <script>
                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                        (function() {
                            'use strict';
                            window.addEventListener('load', function() {
                                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                var forms = document.getElementsByClassName('needs-validation');
                                // Loop over them and prevent submission
                                var validation = Array.prototype.filter.call(forms, function(form) {
                                    form.addEventListener('submit', function(event) {
                                        if (form.checkValidity() === false) {
                                            event.preventDefault();
                                            event.stopPropagation();
                                        }
                                        form.classList.add('was-validated');
                                    }, false);
                                });
                            }, false);
                        })();
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>