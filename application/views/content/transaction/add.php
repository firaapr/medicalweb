<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-anchor icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>REKAM MEDIS</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <form class="needs-validation" action="<?php echo base_url("index.php/transaction/fetch_add"); ?>" method="post"
                    novalidate>
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-11 mb-3">
                                    <div class="position-relative form-group">
                                        <label for="exampleCustomSelect" class="">Pilih Pasien :</label>
                                        <select type="select" id="validationCustom01" name="ID_Patient"
                                            class="custom-select" required>
                                            <option value="">Pilih satu</option>
                                            <?php if (isset($dataPatient) != false && empty($dataPatient) == false) {
                                                $i = 0;
                                                foreach ($dataPatient as $data[]) { ?>
                                            <option value="<?= $data[$i]['ID_Patient'] ?>">
                                                <?= $data[$i]['Name_Patient'] ?>
                                            </option>
                                            <?php $i++; }
                                                    } ?>
                                        </select>
                                    </div>
                                    <div class="invalid-feedback">
                                        Pilih pasien
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-11 mb-3">
                                    <div class="position-relative form-group">
                                        <label for="exampleCustomSelect" class="">Pilih Dokter :</label>
                                        <select type="select" id="validationCustom02" name="ID_Doctor"
                                            class="custom-select" required>
                                            <option value="">Pilih satu</option>
                                            <?php if (isset($dataDoctor) != false && empty($dataDoctor) == false) {
                                                $i = 0;
                                                foreach ($dataDoctor as $dataD[]) { ?>
                                            <option value="<?= $dataD[$i]['ID_Doctor'] ?>">
                                                <?= $dataD[$i]['Name_Doctor'] ?>
                                            </option>
                                            <?php $i++; }
                                                    } ?>
                                        </select>
                                    </div>
                                    <div class="invalid-feedback">
                                        Pilih dokter
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-5 mb-3">
                                    <label for="validationCustom02">Tanggal Periksa</label>
                                    <input type="date" class="form-control" id="validationCustom02" placeholder=""
                                        value="" name="Date" required>
                                    <div class="invalid-feedback">
                                        Masukkan tanggal periksa
                                    </div>
                                </div>
                                <div class="col-md-1 mb-3"></div>
                                <div class="col-md-5 mb-3">
                                    <label for="validationCustom03">Harga</label>
                                    <input type="text" class="form-control" id="validationCustom03" placeholder=""
                                        value="" name="Price" required>
                                    <div class="invalid-feedback">
                                        Masukkan harga pemeriksaan
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-5 mb-3">
                                    <label for="validationCustom02">Diagnosis</label>
                                    <input type="text" class="form-control" id="validationCustom02" placeholder=""
                                        value="" name="Diagnosis" required>
                                    <div class="invalid-feedback">
                                        Masukkan diagnosis penyakit
                                    </div>
                                </div>
                                <div class="col-md-1 mb-3"></div>
                                <div class="col-md-5">
                                    <div class="position-relative form-group">
                                        <label for="exampleText" class="">Keterangan</label>
                                        <textarea name="Diagnosis_Description" id="validationCustom04"
                                            class="form-control" required></textarea>
                                        <div class="invalid-feedback">
                                            Masukkan keterangan diagnosis
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- <button class="btn btn-primary" type="submit">Submit</button> -->
                        </div>
                    </div>

                    <div class="main-card mb-3 card">
                        <div class="card-body" id="content-medicine">
                            <div class="form-row">
                                <div class="col-md-2">
                                    <label for="exampleText" class="">Resep Obat :</label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-11 mb-3">
                                    <table class="mb-0 table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Obat</th>
                                                <th>Jumlah</th>
                                                <th>Pilih</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($dataObat) != false && empty($dataObat) == false) {
                                    $i = 0;
                                    foreach ($dataObat as $dataO[]) { ?>
                                            <tr>
                                                <th scope="row"><?= $i+1 ?></th>
                                                <td><?= $dataO[$i]['Name_Medicine'] ?></td>
                                                <td>
                                                    <input placeholder="Jumlah (per pack)"
                                                        id="total_<?= $dataO[$i]['ID_Medicine'] ?>" type="number"
                                                        name="total" value="" class="mb-2 form-control-sm form-control">
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="cbO"
                                                        value="<?= $dataO[$i]['ID_Medicine'] ?>"
                                                        onclick="checkCp(this)">
                                                </td>
                                            </tr>
                                            <?php $i++; }
                                    } else { ?>
                                            <tr>
                                                <td colspan="5">No result found</td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <input type="hidden" name="dataObat" id="id_obat" value="">
                            <button id="submit" class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

            <script>
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    var forms = document.getElementsByClassName('needs-validation');
                    var validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();

            var selected = [];

            function checkCp(checkbox) {
                var id = checkbox.value;
                var total = document.getElementById("total_" + id).value;
                if (checkbox.checked) {
                    selected.push(total + "-" + id);
                    var test = $('input[name="dataObat"]:hidden');
                    test.val(selected);

                } else {
                    selected.splice(selected.indexOf(id), 1);
                }
            }
            </script>
        </div>
    </div>
</div>

</div>
</div>
</div>