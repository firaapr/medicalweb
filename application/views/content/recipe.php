<div class="app-main__outer">
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-anchor icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>RESEP
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Total Resep</div>
                        <div class="widget-subheading"></div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span>
                                <?= sizeof($data) ?>
                            </span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">List Data</h5>
                    <table class="mb-0 table table-hover" id="tblData">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Obat</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($data) != false && empty($data) == false) {
                            $i = 0;
                            foreach ($data as $data[0][]) { ?>
                            <tr>
                                <th scope="row"><?= $i+1 ?></th>
                                <td><?= $data[$i]['Name_Medicine'] ?></td>
                                <td><?= $data[$i]['Total'] ?></td>
                            </tr>
                            <?php $i++; }
                            } else { ?>
                            <tr>
                                <td colspan="5">No result found</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
</div>



<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script type="text/javascript">
$(document).ready( function () {
    $('#tblData').DataTable();
} );
</script>
