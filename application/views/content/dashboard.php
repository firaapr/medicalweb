<div class="app-main__outer">
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-anchor icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>DASHBOARD REKAM MEDIS
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-4">
            <div class="card mb-3 widget-content bg-midnight-bloom">
                <div class="widget-content-wrapper text-white">
                    <div class="widget-content-left">
                        <div class="widget-heading">Total Rekam Medis</div>
                        <div class="widget-subheading"></div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span>
                                <?= sizeof($data) ?>
                            </span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <h5 class="card-title">List Data</h5>
                    <table class="mb-0 table table-hover" id="tblData">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pasien</th>
                                <th>Dokter</th>
                                <th>Diagnosa</th>
                                <th>Deskripsi</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (isset($data) != false && empty($data) == false) {
                            $i = 0;
                            foreach ($data as $data[0][]) { ?>
                            <tr>
                                <th scope="row"><?= $i+1 ?></th>
                                <td><?= $data[$i]['Name_Patient'] ?></td>
                                <td><?= $data[$i]['Name_Doctor'] ?></td>
                                <td><?= $data[$i]['Diagnosis'] ?></td>
                                <td><?= $data[$i]['Diagnosis_Description'] ?></td>
                                <td><?= $data[$i]['Date'] ?></td>
                                <td>
                                    <button
                                        onclick="location.href='<?= base_url().'index.php/main/view_resep/'.$data[$i]['ID_MedicalRecord'] ?>';"
                                        class="mb-2 mr-2 border-0 btn-transition btn btn-outline-primary">
                                        Lihat Resep
                                    </button>
                                </td>
                            </tr>
                            <?php $i++; }
                            } else { ?>
                            <tr>
                                <td colspan="5">No result found</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p class="mb-0">Apakah Anda yakin ingin menghapus data ini?</p>
        </div>
        <form method="post" action="<?= base_url().'index.php/doctor/action_delete' ?>">
            <input type="hidden" name="id" id="del_id" value="">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Yakin</button>
            </div>
        </form>
    </div>
</div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>
<script type="text/javascript">
$(document).ready( function () {
    $('#tblData').DataTable();
} );
function openModalDelete(id){
    $('#exampleModal').modal('show');
    $("#del_id").val(id);
}
</script>