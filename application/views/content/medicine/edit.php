<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-anchor icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>UBAH OBAT</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form class="needs-validation"
                            action="<?php echo base_url('index.php/medicine/action_edit/'.$data[0]['ID_Medicine']); ?>"
                            method="post" novalidate>
                            <div class="form-row">
                                <div class="col-md-11 mb-3">
                                    <label for="validationCustom01">Nama Obat</label>
                                    <input type="text" class="form-control" id="validationCustom01"
                                        placeholder="Masukkan nama obat" value="<?= $data[0]['Name_Medicine'] ?>"
                                        name="Name_Medicine" required>
                                    <div class="invalid-feedback">
                                        Masukkan nama obat
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-4 mb-3">
                                    <label for="validationCustom02">Tanggal Kadaluarsa</label>
                                    <input type="date" class="form-control" id="validationCustom02" placeholder=""
                                        value="<?= $data[0]['Expired_Date'] ?>" name="Expired_Date" required>
                                    <div class="invalid-feedback">
                                        Masukkan tanggal kadaluarsa
                                    </div>
                                </div>
                                <div class="col-md-1 mb-3"></div>
                                <div class="col-md-6">
                                    <input type="hidden" class="selectedVal" id="ctgVal" placeholder=""
                                        value="<?= $data[0]['Category'] ?>">
                                    <div class="position-relative form-group"><label for="exampleCustomSelect"
                                            class="">Kategori Obat</label><select type="select" id="ctgSelect"
                                            name="Category" class="custom-select" required>
                                            <option>Pilih satu</option>
                                            <option value="Sirup">Sirup</option>
                                            <option value="Kapsul">Kapsul</option>
                                            <option value="Tablet">Tablet</option>
                                            <option value="Oles">Oles (Luka Luar)</option>
                                        </select>
                                    </div>
                                    <div class="invalid-feedback">
                                        Pilih kategori
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Simpan Data</button>
                        </form>

                        <script>
                        (function() {
                            'use strict';
                            window.addEventListener('load', function() {
                                var ctg = document.getElementById('ctgSelect');
                                var val = document.getElementById('ctgVal').value;
                                if (val == 'Sirup') {
                                    ctg.selectedIndex = "1"
                                } else if (val == 'Kapsul') {
                                    ctg.selectedIndex = "2"
                                } else if (val == 'Tablet') {
                                    ctg.selectedIndex = "3"
                                } else if (val == 'Oles') {
                                    ctg.selectedIndex = "4"
                                }

                                var forms = document.getElementsByClassName('needs-validation');
                                var validation = Array.prototype.filter.call(forms, function(form) {
                                    form.addEventListener('submit', function(event) {
                                        if (form.checkValidity() === false) {
                                            event.preventDefault();
                                            event.stopPropagation();
                                        }
                                        form.classList.add('was-validated');
                                    }, false);
                                });
                            }, false);
                        })();
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>