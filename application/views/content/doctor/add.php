<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-anchor icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>TAMBAH DOKTER</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form class="needs-validation" action="<?php echo base_url('index.php/doctor/action_add'); ?>"
                            method="post" novalidate>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom01">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="validationCustom01"
                                        placeholder="Masukkan nama lengkap" value="" name="name" required>
                                    <div class="invalid-feedback">
                                        Masukkan nama lengkap
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom02">Spesialis</label>
                                    <input type="text" class="form-control" id="validationCustom02"
                                        placeholder="Masukkan spesialis" value="" name="special" required>
                                    <div class="invalid-feedback">
                                        Masukkan spesialis
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom03">Alamat</label>
                                    <input type="text" class="form-control" id="validationCustom03"
                                        placeholder="Masukkan alamat" value="" name="address" required>
                                    <div class="invalid-feedback">
                                        Masukkan alamat
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom04">Kota</label>
                                    <input type="text" class="form-control" id="validationCustom04"
                                        placeholder="Masukkan kota" value="" name="city" required>
                                    <div class="invalid-feedback">
                                        Masukkan kota
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="validationCustom05">Telepon</label>
                                    <input type="text" class="form-control" id="validationCustom05"
                                        placeholder="Masukkan telepon" value="" name="phone" required>
                                    <div class="invalid-feedback">
                                        Masukkan telepon
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Tambah</button>
                        </form>

                        <script>
                        // Example starter JavaScript for disabling form submissions if there are invalid fields
                        (function() {
                            'use strict';
                            window.addEventListener('load', function() {
                                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                                var forms = document.getElementsByClassName('needs-validation');
                                // Loop over them and prevent submission
                                var validation = Array.prototype.filter.call(forms, function(form) {
                                    form.addEventListener('submit', function(event) {
                                        if (form.checkValidity() === false) {
                                            event.preventDefault();
                                            event.stopPropagation();
                                        }
                                        form.classList.add('was-validated');
                                    }, false);
                                });
                            }, false);
                        })();
                        </script>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>