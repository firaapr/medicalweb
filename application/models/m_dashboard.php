<?php
class M_dashboard extends CI_Model{
    

    function get_data(){		
        $sql = "SELECT * FROM `medical_record` m INNER JOIN patient p ON m.`ID_Patient` = p.`ID_Patient` INNER JOIN doctor d ON m.`ID_Doctor` = d.`ID_Doctor` INNER JOIN receptionist r ON m.`ID_Receptionist` = r.`ID_Receptionist`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_data_recipe($id){		
        $sql = "SELECT * FROM `medicine_recipe` mr INNER JOIN medicine m ON mr.`ID_Medicine` = m.`ID_Medicine` WHERE mr.`ID_MedicalRecord` = " . $id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
?>
