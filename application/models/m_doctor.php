<?php
class M_doctor extends CI_Model{
    function get_data(){		
        $sql = "select * from doctor";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_data_byid($id){		
        $sql = "select * from doctor where ID_Doctor = ".$id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function input_data($data,$table){
		$this->db->insert($table,$data);
    }
    
    function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
    }	
    
    function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>