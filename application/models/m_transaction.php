<?php
class M_transaction extends CI_Model{
  function get_patient() {		
    $sql = "select * from patient";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_doctor() {		
    $sql = "select * from doctor";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_medicine(){		
    $sql = "select * from medicine";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function input_data_medis($data,$table) {
    $this->db->insert($table,$data);
    return $this->db->insert_id();
    // if($this->db->affected_rows() > 0) {
    //   $query = "select LAST_INSERT_ID()";
    //   $query = $this->db->query($query);
    //   return $query->result();
    // } else {
    //   return false;
    // }
  }

  function get_last_id() {
    $query = "select LAST_INSERT_ID()";
	  $this->db->query($query);
  }

  function input_data_resep($lastId, $id,$total) {
    $query = "INSERT INTO `medicine_recipe`(`ID_MedicalRecord`, `ID_Medicine`, `Total`) VALUES (".$lastId.",".$id.",".$total.")";
	  $this->db->query($query);
  }
}
?>