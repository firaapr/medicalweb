<?php
class M_patient extends CI_Model{
    function get_data(){		
        $sql = "select * from patient";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_data_byid($id){		
        $sql = "select * from patient where ID_Patient = ".$id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function input_data($data,$table){
		$this->db->insert($table,$data);
    }
    
    function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
    }	
    
    function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>